# Projet Blog

L'objectif du projet est de créer une petite application de blog sans authentification en utilisant Spring Boot pour le backend et Angular pour le frontend.

[Lien vers le dépot frontend](https://gitlab.com/d.goubet81/blog-front)