package co.simplo.promo18.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplo.promo18.blog.entity.Category;
import co.simplo.promo18.blog.repository.CategoryRepository;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

  @Autowired
  private CategoryRepository catRepo;

  @GetMapping
  public List<Category> getAll() {
    return catRepo.findAll();
  }

  @GetMapping("/{id}")
  public Category getById(@PathVariable int id) {
    Category category = catRepo.findById(id);
    if (category == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return category;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Category add(@RequestBody Category category) {
    catRepo.save(category);
    return category;
  }

  @PutMapping("/{id}")
  public Category update(@RequestBody Category category, @PathVariable int id) {
    if (id != category.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!catRepo.update(category)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return catRepo.findById(category.getId());
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    if (!catRepo.deleteById(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }
}
