package co.simplo.promo18.blog.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplo.promo18.blog.entity.Post;
import co.simplo.promo18.blog.repository.PostRepository;

@RestController
@RequestMapping("/api/post")
public class PostController {

  @Autowired
  private PostRepository postRepo;

  @GetMapping
  public List<Post> all(@RequestParam Optional<String> title, @RequestParam Optional<String> text,
      @RequestParam Optional<Integer> catId, @RequestParam Optional<String> startDate,
      @RequestParam Optional<String> endDate) {
    if (title.isPresent() || text.isPresent()) {
      return postRepo.search(title, text, catId, startDate, endDate);
    }
    if (catId.isPresent()) {
      return postRepo.findByCategory(catId.get());
    }
    return postRepo.findAllWithCategory();
  }

  @GetMapping("/{id}")
  public Post getById(@PathVariable int id) {
    Post post = postRepo.findByIdWithCategory(id);
    if (post == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return post;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Post add(@Valid @RequestBody Post post) {
    if (post.getDate() == null) {
      post.setDate(LocalDate.now());
    }
    postRepo.save(post);
    return post;
  }

  @PutMapping("/{id}")
  public Post update(@RequestBody Post post, @PathVariable int id) {
    if (id != post.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!postRepo.update(post)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return postRepo.findById(post.getId());
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    if (!postRepo.deleteById(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }
}
