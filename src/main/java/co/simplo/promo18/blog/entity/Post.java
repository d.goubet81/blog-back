package co.simplo.promo18.blog.entity;

import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Post {
  private Integer id;

  @NotBlank
  private String title;

  @NotBlank
  private String text;

  private LocalDate date;

  @NotNull
  private Category category;

  private String image;

  public Post() {}

  public Post(String title, String text, LocalDate date, Category category, String image) {
    this.title = title;
    this.text = text;
    this.date = date;
    this.category = category;
    this.image = image;
  }

  public Post(Integer id, String title, String text, LocalDate date, String image) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.date = date;
    this.image = image;
  }

  public Post(Integer id, String title, String text, LocalDate date, Category category,
      String image) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.date = date;
    this.category = category;
    this.image = image;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }
}
