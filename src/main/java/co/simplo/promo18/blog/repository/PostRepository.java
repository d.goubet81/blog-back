package co.simplo.promo18.blog.repository;

import co.simplo.promo18.blog.entity.Category;
import co.simplo.promo18.blog.entity.Post;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PostRepository {

  @Autowired
  private DataSource dataSource;

  public List<Post> findAll() {
    List<Post> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM post");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Post post = new Post(rs.getInt("id"), rs.getString("title"), rs.getString("text"),
            rs.getDate("date").toLocalDate(), rs.getString("image"));
        list.add(post);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<Post> findAllWithCategory() {
    List<Post> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM post p LEFT JOIN category c ON p.id_category = c.id ORDER BY p.date DESC");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Post post = new Post(rs.getInt("p.id"), rs.getString("p.title"), rs.getString("p.text"),
            rs.getDate("p.date").toLocalDate(),
            new Category(rs.getInt("c.id"), rs.getString("c.label")), rs.getString("p.image"));

        list.add(post);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<Post> findByCategory(int id) {
    List<Post> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM post p LEFT JOIN category c ON p.id_category = c.id WHERE c.id LIKE ? ORDER BY p.date DESC");

      stmt.setString(1, "%" + id + "%");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Post post = new Post(rs.getInt("p.id"), rs.getString("p.title"), rs.getString("p.text"),
            rs.getDate("p.date").toLocalDate(),
            new Category(rs.getInt("c.id"), rs.getString("c.label")), rs.getString("p.image"));

        list.add(post);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<Post> search(Optional<String> title, Optional<String> text,
      Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate) {
    List<Post> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM post p LEFT JOIN category c ON p.id_category = c.id WHERE p.title LIKE ? AND p.text LIKE ? AND c.id LIKE ? AND p.date BETWEEN ? AND ? ORDER BY p.date DESC");

      if (title.isPresent()) {
        stmt.setString(1, "%" + title.get() + "%");
      } else {
        stmt.setString(1, "%");
      }
      if (text.isPresent()) {
        stmt.setString(2, "%" + text.get() + "%");
      } else {
        stmt.setString(2, "%");
      }
      if (categoryId.isPresent()) {
        stmt.setInt(3, categoryId.get());
      } else {
        stmt.setString(3, "%");
      }
      if (startDate.isPresent()) {
        stmt.setDate(4, Date.valueOf(startDate.get()));
      } else {
        stmt.setString(4, "%");
      }
      if (endDate.isPresent()) {
        stmt.setDate(5, Date.valueOf(endDate.get()));
      } else {
        stmt.setDate(5, Date.valueOf(LocalDate.now()));
      }

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Post post = new Post(rs.getInt("p.id"), rs.getString("p.title"), rs.getString("p.text"),
            rs.getDate("p.date").toLocalDate(),
            new Category(rs.getInt("c.id"), rs.getString("c.label")), rs.getString("p.image"));

        list.add(post);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public Post findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM post WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Post post = new Post(rs.getInt("id"), rs.getString("title"), rs.getString("text"),
            rs.getDate("date").toLocalDate(), rs.getString("image"));
        return post;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public Post findByIdWithCategory(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM post p LEFT JOIN category c ON p.id_category = c.id WHERE p.id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Post post = new Post(rs.getInt("p.id"), rs.getString("p.title"), rs.getString("p.text"),
            rs.getDate("p.date").toLocalDate(),
            new Category(rs.getInt("c.id"), rs.getString("c.label")), rs.getString("p.image"));
        return post;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void save(Post post) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO post (title, text, date, id_category, image) VALUES (?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, post.getTitle());
      stmt.setString(2, post.getText());
      stmt.setDate(3, Date.valueOf(post.getDate()));
      stmt.setInt(4, post.getCategory().getId());
      stmt.setString(5, post.getImage());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        post.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean update(Post post) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE post SET title=?, text=?, date=?, id_category=?, image=? WHERE id=?");

      stmt.setString(1, post.getTitle());
      stmt.setString(2, post.getText());
      stmt.setDate(3, java.sql.Date.valueOf(post.getDate()));
      stmt.setInt(4, post.getCategory().getId());
      stmt.setString(5, post.getImage());
      stmt.setInt(6, post.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM post WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}
