-- CREATE DATABASE projet_blog;

DROP TABLE IF EXISTS post;

DROP TABLE IF EXISTS category;

CREATE TABLE category (
  id TINYINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  label VARCHAR(64) NOT NULL
);

INSERT INTO
  category (label)
VALUES
  ("Cuisine"),
  ("Nature"),
  ("Animaux"),
  ("Voyage"),
  ("Loisir"),
  ("Concert");

CREATE TABLE post (
  id SMALLINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(128) NOT NULL,
  text TEXT NOT NULL,
  date DATE NOT NULL,
  id_category TINYINT UNSIGNED,
  image VARCHAR(255),
  CONSTRAINT FK_post_category FOREIGN KEY (id_category) REFERENCES category(id) ON DELETE
  SET
    NULL
);

INSERT INTO
  post (title, text, date, id_category, image)
VALUES
  (
    "Trop mignon!",
    "Ce petit chat qui joue avec cette fleur est vraiment trop mignon!",
    "2022-02-23",
    3,
    "kitten.jpg"
  ),
  (
    "Trop bon!",
    "Ce plat que j'ai mangé hier soir était délicieux!",
    "2022-04-05",
    1,
    "noodle.jpg"
  ),
  (
    "Prêt pour Halloween!",
    "On va pouvoir bien s'éclater avec toutes ces citrouilles. Frisson garanti!",
    "2021-10-17",
    2,
    "pumpkin.jpg"
  ),
  (
    "J'ai plus d'oreille",
    "Le concert d'hier soir était trop génialissime!",
    "2022-05-03",
    6,
    "guitar.jpg"
  );