package co.simplo.promo18.blog.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import co.simplo.promo18.blog.entity.Category;

@SpringBootTest
@Sql({"/database.sql"})
public class CategoryRepositoryTest {

  @Autowired
  CategoryRepository repo;

  @Test
  void testDeleteById() {
    boolean test = repo.deleteById(2);

    assertThat(test).isTrue();
  }

  @Test
  void testDeleteByIdNotExists() {
    boolean test = repo.deleteById(20);

    assertThat(test).isFalse();
  }

  @Test
  void testFindAll() {
    List<Category> result = repo.findAll();
    assertThat(result).hasSize(6).doesNotContainNull()
        .allSatisfy(category -> assertThat(category).hasNoNullFieldsOrProperties());
  }

  @Test
  void testFindById() {
    Category result = repo.findById(1);
    assertThat(result).hasNoNullFieldsOrProperties().hasFieldOrPropertyWithValue("label",
        "Cuisine");
  }

  @Test
  void testSave() {
    Category category = new Category("Test");

    repo.save(category);

    assertThat(category.getId()).isNotNull();
  }

  @Test
  void testUpdate() {
    Category category = new Category(2, "Test");

    boolean test = repo.update(category);

    assertThat(test).isTrue();
  }
}
