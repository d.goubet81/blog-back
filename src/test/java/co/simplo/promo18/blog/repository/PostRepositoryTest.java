package co.simplo.promo18.blog.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import co.simplo.promo18.blog.entity.Category;
import co.simplo.promo18.blog.entity.Post;

@SpringBootTest
@Sql({"/database.sql"})
public class PostRepositoryTest {

  @Autowired
  PostRepository repo;

  @Test
  void testDeleteById() {
    boolean test = repo.deleteById(2);

    assertThat(test).isTrue();
  }

  @Test
  void testFindAll() {
    List<Post> result = repo.findAll();
    assertThat(result).hasSize(4).doesNotContainNull()
        .allSatisfy(post -> assertThat(post).hasNoNullFieldsOrPropertiesExcept("category"));
  }

  @Test
  void testFindById() {
    Post result = repo.findByIdWithCategory(1);
    assertThat(result).hasNoNullFieldsOrPropertiesExcept("category")
        .hasFieldOrPropertyWithValue("title", "Trop mignon!")
        .hasFieldOrPropertyWithValue("text",
            "Ce petit chat qui joue avec cette fleur est vraiment trop mignon!")
        .hasFieldOrPropertyWithValue("date", LocalDate.of(2022, 02, 23))
        .hasFieldOrPropertyWithValue("image", "kitten.jpg");
  }

  @Test
  void testSave() {
    Post post = new Post("Test", "TestTest", LocalDate.parse("2022-05-01"), new Category(1, ""),
        "Test.jpg");

    repo.save(post);

    assertThat(post.getId()).isNotNull();
  }

  @Test
  void testUpdate() {
    Post post = new Post(2, "Test", "TestTest", LocalDate.parse("2022-05-01"), new Category(1, ""),
        "Test.jpg");

    boolean test = repo.update(post);

    assertThat(test).isTrue();
  }

  @Test
  void testFindAllWithCategory() {
    List<Post> result = repo.findAllWithCategory();
    assertThat(result).hasSize(4).doesNotContainNull()
        .allSatisfy(post -> assertThat(post).hasNoNullFieldsOrProperties());
  }

  @Test
  void testFindByIdWithCategory() {
    Post result = repo.findByIdWithCategory(1);
    assertThat(result).hasNoNullFieldsOrProperties()
        .hasFieldOrPropertyWithValue("title", "Trop mignon!")
        .hasFieldOrPropertyWithValue("text",
            "Ce petit chat qui joue avec cette fleur est vraiment trop mignon!")
        .hasFieldOrPropertyWithValue("date", LocalDate.of(2022, 02, 23))
        .hasFieldOrPropertyWithValue("category.id", 3)
        .hasFieldOrPropertyWithValue("category.label", "Animaux")
        .hasFieldOrPropertyWithValue("image", "kitten.jpg");
  }
}
